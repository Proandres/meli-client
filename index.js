/* eslint-disable lodash/prefer-lodash-method, global-require, camelcase */
require('dotenv').config({path: `${__dirname}/.env`});
const webpack = require('webpack');
const express = require('express');
const fs = require('fs');

const {
    PORT,
    NODE_ENV
} = process.env;

const publicPath = `${__dirname}/dist`;

const logger = require('./server/helpers/logger');

const app = express();
const internalApp = express();

app.use(internalApp);
const startApp = () => app.listen(PORT, () => logger.info(`\nApp running on port ${PORT} in ${NODE_ENV} environment`));
logger.info(`\nApp running on port ${PORT} in ${NODE_ENV} environment`);

if (NODE_ENV === 'development') {
    (async () => {
        try {
            fs.rmdirSync(publicPath, {recursive: true});
        // eslint-disable-next-line
        } catch (err) {}
        const middleware = require('webpack-dev-middleware');
        const compiler = webpack(require('./webpack.config'));

        const webpackApp = express();
        webpackApp.use(express.static(publicPath));
        webpackApp.use('*', express.static(publicPath));

        webpackApp.use(middleware(compiler, {
            methods: ['GET', 'POST', 'HEAD', 'PUT', 'DELETE'],
            writeToDisk: false
        }));

        webpackApp.use(require('webpack-hot-middleware')(compiler));
        app.use(webpackApp);
        startApp();
    })();
}
