/**
 * @jest-environment jsdom
 */
import {Items} from '@core/actions';
import fromState from '@selectors';

import store from './store';

const {
    fetchItemsSucceeded,
    fetchItemSucceeded
} = Items;

describe('Items', () => {
    it('FETCH_ITEMS_SUCCEEDED', () => {
        const items = [
            {
                id: 'MLA920035728',
                title: 'Buzo Undefined Not For Sale Talle M',
                price: {
                    currency: 'ARS',
                    amount: 19000,
                    decimals: 0
                },
                picture: 'http://http2.mlstatic.com/D_929974-MLA45882627003_052021-O.jpg',
                condition: 'new',
                free_shipping: true,
                location: 'Buenos Aires',
                category: 'MLA109085'
            },
            {
                id: 'MLA908955284',
                title: 'Cortadora De Pelo Winco W617 220v',
                price: {
                    currency: 'ARS',
                    amount: 1121,
                    decimals: 0
                },
                picture: 'http://http2.mlstatic.com/D_995948-MLA44361265126_122020-I.jpg',
                condition: 'new',
                free_shipping: false,
                location: 'Capital Federal',
                category: 'MLA5411'
            },
            {
                id: 'MLA919141716',
                title: 'Licuadora Smartlife Sl-bl1008 1.5 L Negra Con Jarra De Vidrio 220v - 240v',
                price: {
                    currency: 'ARS',
                    amount: 4751,
                    decimals: 0
                },
                picture: 'http://http2.mlstatic.com/D_955529-MLA45172712014_032021-I.jpg',
                condition: 'new',
                free_shipping: true,
                location: 'Capital Federal',
                category: 'MLA104680'
            },
            {
                id: 'MLA921846292',
                title: 'Plancha A Vapor Philco Pvp1117e Color Blanco 220v',
                price: {
                    currency: 'ARS',
                    amount: 2490,
                    decimals: 0
                },
                picture: 'http://http2.mlstatic.com/D_635215-MLA42418941194_062020-I.jpg',
                condition: 'new',
                free_shipping: false,
                location: 'Capital Federal',
                category: 'MLA10115'
            }
        ];
        store.dispatch(fetchItemsSucceeded({items}));
        const documents = fromState.Items.getItems(store.getState());
        expect(items).toEqual(documents);
    });

    it('FETCH_ITEM_SUCCEEDED', () => {
        const item = {
            id: 'MLA921846292',
            title: 'Plancha A Vapor Philco Pvp1117e Color Blanco 220v',
            price: {
                currency: 'ARS',
                amount: 2490,
                decimals: 0
            },
            picture: 'http://http2.mlstatic.com/D_635215-MLA42418941194_062020-I.jpg',
            condition: 'new',
            free_shipping: false,
            location: 'Capital Federal',
            category: 'MLA10115'
        };

        store.dispatch(fetchItemSucceeded({...item}));
        const document = fromState.Items.getItem(store.getState());
        expect(item).toEqual(document);
    });
});
