/**
 * @jest-environment jsdom
 */
/* eslint-disable no-unused-vars */
import {Session} from '@core/actions';
import fromState from '@selectors';
import store from './store';

const {
    setTextSearch,
    setRequestFlag,
    setStatusMessage
} = Session;

describe('Session', () => {
    it('SET_TEXT_SEARCH', () => {
        const expectedValue = 'top';
        store.dispatch(setTextSearch({textSearch: 'top'}));
        const text = fromState.Session.getTextSearch(store.getState());
        expect(expectedValue).toEqual(text);
    });

    it('SET_REQUEST_FLAG', () => {
        const expectedValue = true;
        store.dispatch(setRequestFlag(true));
        const text = fromState.Session.getFlagData(store.getState());
        expect(expectedValue).toEqual(text);
    });

    it('SET_STATUS_MESSAGE', () => {
        const expectedValue = {message: 'cargando', status: 'error'};
        store.dispatch(setStatusMessage('error', 'cargando'));
        const text = fromState.Session.getStatusMessage(store.getState());
        expect(expectedValue).toEqual(text);
    });
});
