import {
    createStore,
    applyMiddleware,
    compose
} from 'redux';
import history from '@core/middlewares/history';
import middleware from '@core/middlewares';
import rootSaga from '@core/sagas';
import reducers from '@core/reducers';

const composeEnhancers = compose;

const configureStore = preloadedState => createStore(
    reducers(history),
    preloadedState,
    composeEnhancers(
        applyMiddleware(
            ...middleware
        )
    )
);

const store = configureStore();
const [sagaMiddleware] = middleware;
sagaMiddleware.run(rootSaga);

export default store;
