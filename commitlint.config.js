module.exports = {
    rules: {
        'body-leading-blank': [1, 'always'],
        'footer-leading-blank': [1, 'always'],
        'header-max-length': [2, 'always', 100],
        'body-max-line-length': [2, 'always', 80],
        'footer-max-line-length': [2, 'always', 80],
        'scope-case': [2, 'always', 'camel-case'],
    }
};
