import {FETCH_ITEMS_REQUESTED, FETCH_ITEM_SUCCEEDED, FETCH_ITEMS_SUCCEEDED} from './types';

export const fetchItemsRequested = props => ({
    type: FETCH_ITEMS_REQUESTED,
    ...props
});

export const fetchItemSucceeded = props => ({
    type: FETCH_ITEM_SUCCEEDED,
    ...props
});

export const fetchItemsSucceeded = props => ({
    type: FETCH_ITEMS_SUCCEEDED,
    ...props
});
