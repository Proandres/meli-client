/* eslint-disable import/prefer-default-export */
export const LOGOUT = 'LOGOUT';
export const FETCH_LOGIN_REQUESTED = 'FETCH_LOGIN_REQUESTED';
export const FETCH_ITEMS_REQUESTED = 'FETCH_ITEMS_REQUESTED';
export const FETCH_ITEM_SUCCEEDED = 'FETCH_ITEM_SUCCEEDED';
export const FETCH_ITEMS_SUCCEEDED = 'FETCH_ITEMS_SUCCEEDED';
