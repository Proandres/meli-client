/* eslint-disable no-console */
import {
    call,
    put,
    all,
    takeLatest,
    select
} from 'redux-saga/effects';
import replace from 'lodash/replace';
import {Session, Items} from '@actions';
import Api from '@Api/Api';
import {
    ERROR,
    LOADING
} from '@utils/constants';
import {
    getRoutes
} from '@utils';
import fromState from '@selectors';

import {FETCH_ITEMS_REQUESTED} from './types';

const {
    setRequestFlag,
    setStatusMessage
} = Session;
const {
    fetchItemSucceeded,
    fetchItemsSucceeded

} = Items;

const mainRoutes = getRoutes('mainRoutes');

function* fetch({id, push}) {
    try {
        yield put(setRequestFlag(true, LOADING));
        let response;
        const textSearch = yield select(fromState.Session.getTextSearch);

        if (id) {
            response = yield call(Api.get, replace(mainRoutes.item, ':id', id));
            return yield put(fetchItemSucceeded({...response.data}));
        }

        if (textSearch) {
            response = yield call(Api.get, `${mainRoutes.items}?q=${textSearch}`);
            yield put(fetchItemsSucceeded({...response.data}));
            if (!id && push) {
                return push(`${mainRoutes.items}?q=${textSearch}`);
            }
        }
        return true;
    } catch (error) {
        return yield put(setStatusMessage(ERROR, (error.message || error)));
    } finally {
        yield put(setRequestFlag(false, ''));
    }
}

export default function* sessionSagas() {
    yield all([
        takeLatest(FETCH_ITEMS_REQUESTED, fetch)
    ]);
}
