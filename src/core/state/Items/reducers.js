import {
    FETCH_ITEM_SUCCEEDED,
    FETCH_ITEMS_SUCCEEDED
} from './types';

const initialState = {
    item: {
    },
    documents: [],
    categories: []
};

const Session = (state = initialState, {type, ...props}) => {
    switch (type) {
        case FETCH_ITEM_SUCCEEDED:
            return {
                ...state,
                item: props
            };
        case FETCH_ITEMS_SUCCEEDED:
            return {
                ...state,
                documents: props.items,
                categories: props.categories
            };
        default:
            return state;
    }
};

export default Session;
