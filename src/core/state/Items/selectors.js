/* eslint-disable import/prefer-default-export */
import get from 'lodash/get';

export const getItem = state => get(state, 'items.item');
export const getItems = state => get(state, 'items.documents');
export const getCategories = state => get(state, 'items.categories');
