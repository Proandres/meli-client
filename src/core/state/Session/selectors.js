import get from 'lodash/get';
import pick from 'lodash/pick';

// eslint-disable-next-line import/prefer-default-export
export const getFlagData = state => get(state, 'session.flagData');
export const getStatusMessage = state => pick(get(state, 'session'), ['status', 'message']);
export const getTextSearch = state => get(state, 'session.search.textSearch');
