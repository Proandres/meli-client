import {
    SET_REQUEST_FLAG,
    SET_STATUS_MESSAGE,
    SET_TEXT_SEARCH
} from './types';

const initialState = {
    status: '',
    message: null,
    flagData: false,
    search: {
        textSearch: ''
    }
};

const Session = (state = initialState, {type, ...props}) => {
    switch (type) {
        case SET_REQUEST_FLAG:
            return {
                ...state,
                flagData: props.flag,
                type: props.requestType
            };
        case SET_STATUS_MESSAGE:
            return {
                ...state,
                status: props.status,
                message: props.message
            };
        case SET_TEXT_SEARCH:
            return {
                ...state,
                search: {
                    ...state.search,
                    ...props
                }
            };

        default:
            return state;
    }
};

export default Session;
