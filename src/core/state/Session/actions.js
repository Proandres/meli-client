import {
    SET_STATUS_MESSAGE,
    SET_REQUEST_FLAG,
    SET_TEXT_SEARCH
} from './types';

export const setStatusMessage = (status, message) => ({
    type: SET_STATUS_MESSAGE, status, message
});

export const setRequestFlag = (flag, requestType) => ({
    type: SET_REQUEST_FLAG,
    flag,
    requestType
});

export const setTextSearch = textSearch => ({
    type: SET_TEXT_SEARCH,
    ...textSearch
});
