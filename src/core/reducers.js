import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';
import history from './middlewares/history';
import session from './state/Session/reducers';
import items from './state/Items/reducers';

export default () => combineReducers({
    items,
    session,
    router: connectRouter(history)
});
