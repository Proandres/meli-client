import {all} from 'redux-saga/effects';
import items from './state/Items/sagas';

export default function* rootSagas() {
    yield all([
        items()
    ]);
}
