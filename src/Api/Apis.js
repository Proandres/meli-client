/* global API, TOKEN */

export default class Api {
    static async get(URL) {
        const response = await fetch(
            `${API}${URL}`,
            {
                headers: {
                    'Content-Type': ' application/json',
                    Authorization: `Bearer ${TOKEN}`
                },
                method: 'GET'
            }
        );
        return response.json();
    }
}
