// eslint-disable-next-line import/prefer-default-export
export const i18n = {
    new: 'Nuevo',
    productDescription: 'Descripcion del Producto',
    sold: 'Vendidos',
    buy: 'Comprar',
    searcherPlaceHolder: 'Nunca dejes de buscar'
};
