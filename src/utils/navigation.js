const mainRoutes = {
    home: '/',
    items: '/items',
    item: '/items/:id'
};

export default {
    mainRoutes
};
