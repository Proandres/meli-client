import get from 'lodash/get';
import isArray from 'lodash/isArray';
import pick from 'lodash/pick';
import isString from 'lodash/isString';
import map from 'lodash/map';
import replace from 'lodash/replace';
import size from 'lodash/size';
import set from 'lodash/set';
import forEach from 'lodash/forEach';
import last from 'lodash/last';
import rounds from 'lodash/round';
import toNumber from 'lodash/toNumber';
import upperFirst from 'lodash/upperFirst';
import routeContent from './navigation';

export const getRoutes = entity => {
    if (isArray(entity)) {
        return pick(routeContent, entity);
    }
    if (entity) {
        return get(routeContent, entity);
    }

    return routeContent;
};

export const jsonToString = (stringOrJson, toString = false) => {
    try {
        if (toString) {
            return JSON.stringify(stringOrJson);
        }
        if ((!stringOrJson && stringOrJson !== false) || stringOrJson === 'undefined') {
            return null;
        }
        if (isString(stringOrJson)) {
            return JSON.parse(stringOrJson);
        }
        return stringOrJson;
    } catch (error) {
        return stringOrJson;
    }
};

export const convertToParams = formValues => JSON.stringify(formValues);

export const generateDocumentResponse = documents => ({
    resultsCount: size(documents),
    documents: map(documents, doc => {
        forEach(doc, (value, key) => set(doc, key, jsonToString(replace(value, /'/g, '"'))));
        return doc;
    }),
    hasOwnPagination: true
});

export const getUpperFirstLast = collection => upperFirst(last(collection));

export const formatMoney = ({
    amount, format, currency, round
}) => {
    if (amount && toNumber(amount) && amount !== 'null') {
        const newMont = new Intl.NumberFormat(format, {style: 'currency', currency, minimumFractionDigits: 0}).format(rounds(amount, round));
        return replace(newMont, /(\d)(\d{3})/, '$1.$2');
    }
    return '0';
};
