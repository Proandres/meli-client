import React from 'react';
import {useSelector} from 'react-redux';
import Spinners from '@components/Spinners';
import fromState from '@selectors';

export default function ActionFadeDrop() {
    const flagData = useSelector(fromState.Session.getFlagData);
    if (flagData) {
        return (
            <>
                <div
                    className="modal fade show d-flex align-items-center justify-content-center"
                    role="dialog"
                >
                    <h1 className="text-white">
                        <Spinners width="5rem" height="5rem" color="danger"/>
                    </h1>
                </div>
                <div
                    className="modal-backdrop fade show"
                />
            </>
        );
    }
    return null;
}
