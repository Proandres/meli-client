import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {
    Col,
    Row, Media,
    Card, CardImg, CardBody,
    CardTitle, CardSubtitle
} from 'reactstrap';
import get from 'lodash/get';
import replace from 'lodash/replace';
import {
    getRoutes, formatMoney
} from '@utils';
import searchMin from '../../images/ic_shipping.png';

const mainRoutes = getRoutes('mainRoutes');

const Detail = ({
    item
}) => (
    <>
        <Card tag={Link} to={`${replace(mainRoutes.item, ':id', get(item, 'id'))}`}>
            <Row className="p-4">
                <Col md="2">
                    <CardImg
                        top
                        style={{maxHeight: '35vh'}}
                        width="100%"
                        src={get(item, 'picture') || ''}
                        alt="Card image cap"
                    />
                </Col>
                <Col>
                    <CardBody className="pt-2">
                        <CardTitle tag="h2">
                            <Row className="my-auto">
                                <Col md="3" className="pr-0">

                                    {formatMoney({
                                        amount: get(item, 'price.amount'),
                                        format: 'es-AR',
                                        currency: get(item, 'price.currency'),
                                        round: get(item, 'price.decimals')
                                    })}

                                </Col>
                                <Col className="pl-0 mt-1">
                                    {get(item, 'ree_shipping')
                                    && (
                                        <Media
                                            className="logo-app"
                                            alt="search"
                                            src={searchMin}
                                        />
                                    )}
                                </Col>
                            </Row>
                        </CardTitle>
                        <CardTitle tag="h5" className="mb-2 mt-1 text-muted">{get(item, 'title')}</CardTitle>
                    </CardBody>
                </Col>
                <Col className="text-right mt-3">
                    <CardBody>
                        <CardSubtitle tag="h6" className="mb-2 text-muted">{get(item, 'location')}</CardSubtitle>
                    </CardBody>
                </Col>
                <Col md="1"/>
            </Row>

        </Card>
    </>
);

Detail.propTypes = {
    item: PropTypes.shape({
    }).isRequired
};

export default Detail;
