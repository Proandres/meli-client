import React from 'react';
import PropTypes from 'prop-types';
import {
    Breadcrumb,
    BreadcrumbItem,
    Row,
    Col,
    NavLink
} from 'reactstrap';

import kebabCase from 'lodash/kebabCase';
import map from 'lodash/map';
import slice from 'lodash/slice';

const Breadcrumbs = ({categories}) => (
    <Row className="ml-0 mt-4">
        <Col className="my-auto pl-0">
            <Breadcrumb className="breadcrumbs">
                { map(slice(categories, 0, 4), section => (
                    <BreadcrumbItem key={section} >
                        <NavLink className="breadcrumb-link" href={`https://www.mercadolibre.com.ar/${kebabCase(section)}`}>
                            {section}
                        </NavLink>
                    </BreadcrumbItem>
                ))}
            </Breadcrumb>
        </Col>
    </Row>
);

Breadcrumbs.propTypes = {
    categories: PropTypes.shape({}).isRequired
};

export default Breadcrumbs;
