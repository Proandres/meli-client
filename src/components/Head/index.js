import React from 'react';
import PropTypes from 'prop-types';

const Component = ({
    description,
    title,
    uri
}) => (
    <head>
        <meta
            name="title"
            content={title}
        />
        <meta
            property="og:title"
            content={title}
        />
        <meta
            name="description"
            content={description}
        />
        <meta
            property="og:description"
            content={description}
        />
        <meta property="og:image" content={uri}/>
    </head>

);

Component.propTypes = {
    title: PropTypes.func.isRequired,
    description: PropTypes.func.isRequired,
    uri: PropTypes.func.isRequired
};

export default Component;
