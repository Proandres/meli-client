import React from 'react';
import PropTypes from 'prop-types';
import {
    InputGroup, InputGroupAddon, Button, Input, Form, Media
} from 'reactstrap';
import get from 'lodash/get';
import {i18n} from '@utils/traslate';
import searchMin from '../../images/search.png';

const Searcher = ({
    onSearch,
    setFilter,
    textSearch
}) => {
    const handleChange = e => {
        e.preventDefault();
        const {value} = e.target;
        setFilter(value);
    };

    const handleSubmit = e => {
        e.preventDefault();
        onSearch();
    };

    return (
        <Form onSubmit={handleSubmit}>
            <InputGroup className="mt-2 d-flex align-items-center">
                <Input
                    name="name"
                    placeholder={get(i18n, 'searcherPlaceHolder')}
                    onChange={handleChange}
                    value={textSearch}
                />
                <InputGroupAddon addonType="append">
                    <Button
                        className="searcher-button background-color"
                        type="submit"
                    >
                        <Media
                            className="logo-app"
                            alt="search"
                            src={searchMin}
                        />
                    </Button>
                </InputGroupAddon>
            </InputGroup>
        </Form>
    );
};

Searcher.propTypes = {
    onSearch: PropTypes.func.isRequired,
    i18n: PropTypes.shape({}).isRequired,
    setFilter: PropTypes.func.isRequired,
    textSearch: PropTypes.shape({}).isRequired,
    notSearcher: PropTypes.shape({}).isRequired
};

export default Searcher;
