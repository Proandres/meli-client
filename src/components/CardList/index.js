import React from 'react';
import PropTypes from 'prop-types';
import {
    Col,
    Row
} from 'reactstrap';
import map from 'lodash/map';
import isEmpty from 'lodash/isEmpty';
import Card from '@components/Card';

const Detail = ({
    components
}) => (
    <>
        <Row className="mt-4 mb-4">
            {!isEmpty(components) && map(components, item => (
                <Col md="12">
                    <Card
                        item={item}
                    />
                </Col>
            ))}
        </Row>
    </>
);

Detail.propTypes = {
    components: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

export default Detail;
