import React from 'react';
import PropTypes from 'prop-types';
import {
    Col,
    Row,
    Container,
    Media
} from 'reactstrap';

const Component = ({title, subtitle, author}) => (
    <Container>
        <Row className="mt-4">
            <Col className="d-flex justify-content-center">
                <h1 className="mt-4">
                    {title}
                </h1>
            </Col>
        </Row>
        <Row className="mt-4">
            <Col className="d-flex justify-content-center">
                <Media
                    className="logo-app"
                    alt="logo"
                    src="https://http2.mlstatic.com/storage/developers-site-cms-admin/321781592385-366633181898-logo-developers.png"
                />
            </Col>
        </Row>
        <Row className="mt-4">
            <Col className="d-flex justify-content-center">
                <h4>
                    {subtitle}
                </h4>
            </Col>
        </Row>
        <Row className="mt-4">
            <Col className="d-flex justify-content-center">
                <h4>
                    {author}
                </h4>
            </Col>
        </Row>
    </Container>
);

Component.propTypes = {
    title: PropTypes.string,
    subtitle: PropTypes.string,
    author: PropTypes.string
};

Component.defaultProps = {
    author: 'by Andrés Prömmel',
    title: 'Bienvenido a la prueba tecnica de Meli',
    subtitle: 'Esta es la prueba tecnica de Meli, ingresa el termino en el buscador del header para ver los productos disponibles'
};

export default Component;
