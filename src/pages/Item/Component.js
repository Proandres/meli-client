import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {
    Col,
    Row,
    Container,
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button
} from 'reactstrap';
import get from 'lodash/get';
import toLower from 'lodash/toLower';
import Breadcrumbs from '@components/Breadcrumbs';
import Head from '@components/Head';
import {i18n} from '@utils/traslate';
import {
    formatMoney
} from '@utils';

const Component = ({
    fetchItemsRequested,
    match: {params: {id}},
    history: {push},
    item
}) => {
    useEffect(() => {
        fetchItemsRequested({id, push});
    }, [fetchItemsRequested, id, push]);
    const title = get(item, 'title');
    const description = get(item, 'description');
    const picture = get(item, 'picture');
    return (
        <>
            <Head title={title} description={description} uri={picture}/>
            <Container>
                <Breadcrumbs categories={[get(item, 'category')]}/>
                <Row className="mt-4">
                    <Col>
                        <Card className="p-4 card-offer">
                            <Row className="p-4">
                                <Col md="9">
                                    <CardImg
                                        width="100%"
                                        height="400px"
                                        src={picture || ''}
                                        alt="Card image cap"
                                    />
                                </Col>
                                <Col md="3">
                                    <CardSubtitle tag="h6" className="bold mb-2 text-muted">
                                        {`
                                    ${get(i18n, item.condition, item.condition)} - ${toLower(item.sold_quantity)} 
                                    ${get(i18n, 'sold')}
                                    `}
                                    </CardSubtitle>
                                    <CardTitle tag="h2">{title}</CardTitle>
                                    <CardSubtitle tag="h1" className="mt-3 mb-2">
                                        {formatMoney({
                                            amount: get(item, 'price.amount'),
                                            format: 'es-AR',
                                            currency: get(item, 'price.currency')
                                        })}
                                    </CardSubtitle>
                                    <Button color="info" className="w-100 p-2 mt-4">
                                        {get(i18n, 'buy')}
                                    </Button>
                                </Col>
                            </Row>
                            <Row>
                                <Col className="mt-4">
                                    <CardBody>
                                        <CardTitle tag="h5">{get(i18n, 'productDescription')}</CardTitle>
                                        <CardText className="detail-text">
                                            {description}
                                        </CardText>
                                    </CardBody>
                                </Col>
                                <Col md="4"/>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

Component.propTypes = {
    fetchItemsRequested: PropTypes.func.isRequired,
    match: PropTypes.shape({params: PropTypes.shape({id: PropTypes.string})}).isRequired,
    item: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    history: PropTypes.shape({push: PropTypes.func.isRequired}).isRequired
};

export default Component;
