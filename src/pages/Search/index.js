/* eslint-disable no-unused-vars */
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import fromState from '@core/selectors';
import {Items} from '@core/actions';
import Component from './Component';

const {
    fetchItemsRequested
} = Items;

export default connect(
    state => ({
        items: fromState.Items.getItems(state),
        categories: fromState.Items.getCategories(state)
    }),
    dispatch => bindActionCreators({
        fetchItemsRequested
    }, dispatch)
)(Component);
