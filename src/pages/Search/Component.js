import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {
    Col,
    Row,
    Container
} from 'reactstrap';
import Breadcrumbs from '@components/Breadcrumbs';
import CardList from '@components/CardList';

const Component = ({
    fetchItemsRequested,
    items,
    categories,
    history: {push}
}) => {
    useEffect(() => {
        fetchItemsRequested({push});
    }, [fetchItemsRequested, push]);

    return (
        <Container>
            <Breadcrumbs categories={categories}/>
            <Row>
                <Col>
                    <CardList components={items}/>
                </Col>
            </Row>
        </Container>
    );
};

Component.propTypes = {
    fetchItemsRequested: PropTypes.func.isRequired,
    match: PropTypes.shape({params: PropTypes.shape({id: PropTypes.string})}).isRequired,
    items: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    history: PropTypes.shape({push: PropTypes.func.isRequired}).isRequired,
    categories: PropTypes.arrayOf(PropTypes.string).isRequired
};

export default Component;
