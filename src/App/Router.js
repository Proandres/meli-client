import React from 'react';
import {Route, Switch} from 'react-router-dom';
import {Container} from 'reactstrap';
import {getRoutes} from '@utils';

import ActionFadeDrop from '@components/ActionFadeDrop';
import ToastMessage from '@components/ToastMessage';

import Home from '@pages/Home';
import Item from '@pages/Item';
import Search from '@pages/Search';
import Header from './header';
import ErrorBoundary from './ErrorBoundary';

const mainRoutes = getRoutes('mainRoutes');

const Router = () => (
    <>
        <ErrorBoundary>
            <Header/>
            <Container className="background">
                <ActionFadeDrop/>
                <ToastMessage/>
                <Switch>
                    <Route exact path={mainRoutes.home} component={Home}/>
                    <Route exact path={mainRoutes.item} component={Item}/>
                    <Route exact path={mainRoutes.items} component={Search}/>
                    <Route component={Home}/>
                </Switch>
            </Container>
        </ErrorBoundary>
    </>
);
export default Router;
