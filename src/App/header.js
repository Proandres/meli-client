import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
    Media,
    Navbar,
    NavLink,
    Container,
    Row,
    Col
} from 'reactstrap';
import {Link, useHistory} from 'react-router-dom';
import Searcher from '@components/Searcher';
import {Session, Items} from '@core/actions';
import fromState from '@selectors';
import logo from '../images/logo.png';

const {setTextSearch} = Session;

const {fetchItemsRequested} = Items;

const Header = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const {push} = history;
    const textSearch = useSelector(fromState.Session.getTextSearch);

    const onSubmit = async () => {
        dispatch(fetchItemsRequested({push}));
    };

    const onChange = value => {
        dispatch(setTextSearch({textSearch: value}));
    };

    return (
        <>
            <header className="bg mb-2">
                <Container className="background-primary ml-auto mr-auto">
                    <Row>
                        <Col sm="12" md="1" className="p-0">
                            <Navbar expand="md">
                                <NavLink
                                    tag={Link}
                                    className="logo"
                                    to="/"
                                >
                                    <Media
                                        className="logo-app"
                                        alt="logo"
                                        src={logo}
                                    />
                                </NavLink>
                            </Navbar>
                        </Col>
                        <Col sm="12" md="11" className="mt-1 pr-4">
                            <Searcher
                                onSearch={onSubmit}
                                setFilter={onChange}
                                textSearch={textSearch}
                            />
                        </Col>
                    </Row>
                </Container>
            </header>
        </>
    );
};

export default Header;
